// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import {getFirestore, doc, getDoc, setDoc, updateDoc, deleteDoc, getDocs, query, collection, where } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js';


//Tu podać dane do configuracji:
const firebaseConfig = {
//apiKey: 
//authDomain:
projectId: "bookstore-8e1ad",
//storageBucket: 
//messagingSenderId: 
//appId: 
//measurementId: 
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


//Połączenie z bazą danych (data base)
const db = getFirestore(app);

//Nazwa kolekcji
const booksCollection = "Books";

//Odniesienie do dokumentu w bazie ("1" to nadany w firebase indeks)
//const docRef = doc(db, booksCollection, "3");

//Odczytanie dokumentu
// const docSnap = await getDoc(docRef);

// if (docSnap.exists()){
//     console.log(docSnap.data());
// }else{
//     console.log("Book doesn't exist in our database :-(");
// };

//Dodanie książki:

//const bookRef=doc(db, booksCollection, "3");

// const book ={    
//     Author: "Carlos Ruiz Zafón",
//     Available: 0,
//     Rent: 2,
//     Reserved: 1,
//     Title:"Więzien nieba",
//     Total_books: 3
// };

// setDoc (bookRef, book);

//Aktualizacja

const bookForUpdateRef = doc(db, booksCollection, "1")

const bookForUpdate = {
   
    Available: 2,
    Rent: 1,
    Reserved: 2,
    Total_books: 5
}
await updateDoc(bookForUpdateRef, bookForUpdate);

//Usuwanie
await deleteDoc(doc(db, booksCollection, "3"));

//Odczyt wielu elementów
// const q = query(collection(db, booksCollection), where("Available", ">", 0));
// const querySnap = await getDocs(q);
// querySnap.forEach((doc)=>{
//     console.log(doc.data());
// });
//-----------------------------------------------

//Pobranie wszystkich elementów
const allBooks = query(collection(db, booksCollection));
const querySnap =await getDocs(allBooks);
querySnap.forEach((doc) =>{
    const {Author,Title, Available, Rent, Reserved, Total_books} = doc.data();
//console.log (Author);
    document.getElementById("author").textContent=Author;
    document.getElementById("title").textContent=Title;
    document.getElementById("available").textContent=Available;
    document.getElementById("rent").textContent=Rent;
    document.getElementById("reserved").textContent=Reserved;
    document.getElementById("totalBooks").textContent=Total_books;
});


