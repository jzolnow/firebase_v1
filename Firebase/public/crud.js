import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import {
    getAuth,
    signInWithEmailAndPassword,
    AuthErrorCodes,
    createUserWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
    GoogleAuthProvider,
    signInWithRedirect
    
 } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js';

const firebaseConfig = {
      projectId: "bookstore-8e1ad",
      apiKey: "AIzaSyChKJvrl2GbqpjUezpBBP-lgqHwtSjEKIc"
       };

//Inicjalizacja aplikacji:
const app = initializeApp(firebaseConfig);

//Inicjalizacja modułu:
const auth = getAuth(app);

//Dodanie połączenia z el. UI:

const formEmail = document.querySelector("#formEmail");
const formPassword = document.querySelector("#formPassword");
const btnLogin = document.querySelector("#btnLogin");
const btnLogout=document.querySelector("#btnLogout");
const btnSignup = document.querySelector("#btnSignup");
const errorMessage = document.querySelector("#errorMessage");

// const provider = new GoogleAuthProvider();
// const authGoogle = getAuth();
// signInWithRedirect(authGoogle, provider);

//Dodanie funkcjonalności logowania:
const loginUser = async()=>{
    const valueEmail = formEmail.value;
    const valuePassword = formPassword.value;

    try{
        const user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
    }catch (error){
        if(error.code==AuthErrorCodes.INVALID_PASSWORD){
            errorMessage.innerHTML="Wrong password!";
        }
        else{
            errorMessage.innerHTML=error;
        }
    }
}

//Podłączenie funkcji logującej do przyciska:
btnLogin.addEventListener("click", loginUser)

//Dodanie i podłączenie rejestracji:
const createUser = async() =>{
    const valueEmail=formEmail.value;
    const valuePassword=formPassword.value;

    try{
        const user = await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(user.user);
    } catch(error) {
    console.warn(error);
    }
}

btnSignup.addEventListener("click", createUser);

//Wylogowanie:
const logoutUser = async() => {
    await signOut(auth);
    console.log("User logged out")
}

btnLogout.addEventListener("click", logoutUser);

//Obsługa sesji użytkownika
const authObserveState = async() => {
    onAuthStateChanged(auth, user =>{
        if (user) {
            loginForm.style.display = "none"
            userAuth.style.display = "flex"
            collection.style.display = "flex"
        }
        else{
            loginForm.style.display = "flex"
            userAuth.style.display = "none"
            collection.style.display = "none"
        }
    })
}

authObserveState();

